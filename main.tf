resource "docker_container" "prestashop" {
  name  = var.container_name
  image = docker_image.prestashop.latest
  //logs = true
 // attach = true
 env = var.prestashop_env
  //command = ["/usr/bin/import_config", join(", ", var.prestashop_env), var.prestashop_name]
  //tty = false
  //stdin_open = true

  ports {
    internal = 80
    ip = "127.0.0.1"
  }

 volumes {
    volume_name = docker_volume.shared_volume.name
    container_path = var.container_path
    host_path = docker_volume.shared_volume.mountpoint
    read_only = false
  }
  networks_advanced {
    name = var.network_name
  }

}

resource "docker_volume" "shared_volume" {
  name = "${var.container_name}_volume"
}

resource "docker_image" "prestashop" {
  name = var.docker_image
}
