variable "container_name" {
  description = "container name"
}

variable "network_name" {
  description = "network name of the database container"
}

variable "prestashop_env" {
  description = "database config"
  type = set(string)
}
variable "container_path" {
  default = "/var/www/html"
  description = "path to be persisted"
}
variable "prestashop_name" {
  description = "name of the zip file that contains the wordpress configuration in the registry package of the infra-localhost project (without .zip)"
}


variable "docker_image" {
  default = "prestashop/prestashop:1.7.8.7-apache"
  description = "The defaut image of prestashop tested and working"
}